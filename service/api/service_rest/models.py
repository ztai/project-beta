from django.db import models


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17)
    sold = models.BooleanField(default=False)


class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=200)


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=200, default="created")
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=200)

    technician = models.ForeignKey(
        Technician,
        related_name='technician',
        on_delete=models.CASCADE)

    def finish(self):
        self.status = "finished"

    def cancel(self):
        self.status = "canceled"
