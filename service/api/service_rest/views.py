from django.shortcuts import render
import json
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Technician, Appointment, AutomobileVO
from django.views.decorators.http import require_http_methods


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
        ]


class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
    ]


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
        "id"
        ]
    encoders = {
            # "vin": AutomobileVODetailEncoder()
            "technician": TechnicianDetailEncoder()
    }


@require_http_methods(['GET', "POST", "DELETE"])
def technician_list(request, employee_id=None):
    if request.method == "GET":
        technician = Technician.objects.all()
        context = {
            "technician": technician
        }
        return JsonResponse(context, encoder=TechnicianDetailEncoder)
    elif request.method == "POST":
        content = json.loads(request.body)

        try:
            technician = Technician(
                first_name=content["first_name"],
                last_name=content["last_name"],
                employee_id=content["employee_id"],
            )
            technician.save()
            return JsonResponse(content, encoder=TechnicianDetailEncoder)
        except Exception as e:
            return JsonResponse({'error': e.errors}, status=400)
    elif request.method == "DELETE":
        try:
            technician = Technician.objects.get(
                employee_id=employee_id)
            technician.delete()
            return JsonResponse({"Delete": True})
        except Exception as e:
            return JsonResponse({'error': e.errors}, status=400)


@require_http_methods(['GET', "POST", "DELETE"])
def appointment_list(request, id=None):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(appointments, encoder=AppointmentDetailEncoder, safe=False)
    elif request.method == "POST":
        content = json.loads(request.body)
        content["technician"] = Technician.objects.get(employee_id=content["technician"])
        appointment = Appointment(**content)
        appointment.save()
        print(appointment)
        return JsonResponse(appointment, encoder=AppointmentDetailEncoder, safe=False)
    elif request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(
                id=id)
            appointment.delete()
            return JsonResponse({"Deleted": True})
        except Exception as e:
            return print({"error": e})


@require_http_methods(['GET'])
def service_history(request):
    if request.method == "GET":
        form = appointment_list(request.GET)
        if form.is_valid():
            vin = form.cleaned_data["vin"]
            appointments = Appointment.objects.filter(vin=vin)
        else:
            appointments = Appointment.objects.all()

        return render('http://localhost:8100/api/appointments/', {
            'appointments': appointments})


def cancel_appointment(request, id):
    appointment = Appointment.objects.get(id=id)
    appointment.cancel()
    appointment.save()
    return JsonResponse(appointment, encoder=AppointmentDetailEncoder, safe=False)


def finish_appointment(request, id):
    appointment = Appointment.objects.get(id=id)
    appointment.finish()
    appointment.save()
    return JsonResponse(appointment, encoder=AppointmentDetailEncoder, safe=False)
