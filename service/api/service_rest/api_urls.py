from django.urls import path

from .views import appointment_list, technician_list, cancel_appointment, finish_appointment


urlpatterns = [
    path("technicians/<int:employee_id>", technician_list, name="technicians"),
    path("technicians/", technician_list, name="technicians"),
    path("appointments/", appointment_list, name="appointments"),
    path("appointments/<int:id>", appointment_list, name="appointment_list"),
    path("appointments/<int:id>/cancel", cancel_appointment, name="cancel_appointment"),
    path("appointments/<int:id>/finish", finish_appointment, name="finish_appointment")
]
