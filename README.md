# CarCar

Team:

* Tai - Sales Microservice
* Mike  - Service microservice

## Design

CarCar is used through a React front-end that is based around three APIs, Inventory, Service and Sales. Inventory contains the automobiles while service contains technician logs and service history with sales containing salespersons, customers, and sales. Both Service and Sales poll Inventory for it's automobiles.

## Service microservice
The Service microservice is responsible for storing and retrieving data about Services provided from the dealer. The poller is connected through the microservice between Service and Sales. Data is polling in the service microservice. three models have been created for this service, AutomobileVo, technician, and appointments. These are followed by the RestFul API endpoints.  The data is stored and retrieved using our PostgresSQL database. The database contains a table that stores information such as VIN, sold, technician info and appointment information and their field details. In order to translate this data, we are using React Js on the front end interface. This allows our code o be more modular and interactive.

## Sales microservice

Sales microservice is designed around a React frontend and a API based off Django backend. The microservice allows for the viewing, creation, and deletion of Salespeople, Customers, and Sales. The microservice also contains a special Sales history page in which the user can look up the sales of an employee. The API requires the polling of the inventory API's automobiles and runs every minute to generate local AutomobileVOs.
