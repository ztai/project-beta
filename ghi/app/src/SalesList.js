import React, { useEffect, useState } from 'react';

function SalesList() {
    const [sales, setSales] = useState([])


    const fetchData = async () => {

        const url = 'http://localhost:8090/api/sales/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales)
        }
    }

    useEffect(() => {
        fetchData()
    }, []);

    const clickDelete = async (id) => {
        const url = `http://localhost:8090/api/sales/${id}`;
        const fetchConfig = {
            method: "Delete",
        };
        const response = await fetch(url, fetchConfig);
        fetchData()
    }

    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson Employee ID</th>
                        <th>Salesperson Name </th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map(sale => {
                        return (
                            <tr key={sale.automobile.vin}>
                                <td className="align-middle">{sale.salesperson.employee_id}</td>
                                <td className="align-middle">{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                                <td className="align-middle">{sale.customer.first_name} {sale.customer.last_name}</td>
                                <td className="align-middle">{sale.automobile.vin}</td>
                                <td className="align-middle">${sale.price}</td>
                            </tr>
                        )
                    })
                    }
                </tbody>
            </table>
        </>
    );
}

export default SalesList;
