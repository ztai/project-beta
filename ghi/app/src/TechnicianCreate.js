import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';

function TechnicianCreateForm() {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [employeeID, setEmployeeID] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();

    const url = 'http://localhost:8080/api/technicians/';
    const formData = {}
    formData['first_name'] = firstName
    formData["last_name"] = lastName
    formData["employee_id"] = employeeID
    console.log(JSON.stringify(formData))
    fetch(url, {
      method: 'POST',
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    })
      .then((response) => {
        if (response.status === 200) {
          console.log('Technician created successfully');
        } else {
          console.log('Error creating technician');
        }
      })
      .catch((error) => {
        console.error(error);
      });
      setFirstName("");
      setLastName("");
      setEmployeeID("");
  };

  return (
    <div className='row'>
      <div className='offset-3 col-6'>
        <div className='shadow p-4 mt-4'>
          <h1>Add a Technician</h1>
          <form onSubmit={handleSubmit}>
            <div className="form-group mb-3">
              <input type="text" value={firstName} onChange={(e) => setFirstName(e.target.value)} className="form-control" placeholder='First Name...' />
            </div>
            <div className="form-group mb-3">
              <input type="text" value={lastName} onChange={(e) => setLastName(e.target.value)} className="form-control" placeholder='Last Name...' />
            </div>
            <div className="form-group mb-3">
              <input type="text" value={employeeID} onChange={(e) => setEmployeeID(e.target.value)} className="form-control" placeholder='Employee ID...' />
            </div>
            <button type="submit" className="btn btn-primary">Create Technician</button>
          </form>
        </div>
      </div>
    </div>
  );
}
export default TechnicianCreateForm;
