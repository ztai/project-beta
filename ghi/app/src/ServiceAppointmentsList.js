import React, { useEffect, useState } from 'react';



function AppointmentList() {
  const [appointments, setAppointments] = useState([]);

  const fetchAppointments = async () => {
    const url = 'http://localhost:8080/api/appointments/';
    const response = await Promise.resolve(fetch(url));
    if (response.ok) {
      const data = await response.json();
      console.log(data)
      setAppointments(data.filter(appointment => appointment.status=="created")); // created
    }
  };
  const[VIPS, setVIPS] = useState([]);

  const fetchVIP = async () => {
    const url = "http://localhost:8100/api/automobiles/";
    const response = await Promise.resolve(fetch(url));
    if (response.ok) {
      const data = await response.json();
      data.autos.map (auto => auto.vin)
      console.log(data)
      setVIPS(data.autos.map (auto => auto.vin));
      console.log(VIPS)
  }}

  useEffect(() => {
    fetchAppointments();
    fetchVIP();
  }, []);

  const clickCancel = async (id) => {
    const url = `http://localhost:8080/api/appointments/${id}/cancel`;
    const fetchConfig = {
      method: "PUT",
    };
    const response = await fetch(url, fetchConfig);
    fetchAppointments();
  };

  const clickFinish = async (id) => {
    const url = `http://localhost:8080/api/appointments/${id}/finish`;
    const fetchConfig = {
      method: "PUT",
    };
    const response = await fetch(url, fetchConfig);
    fetchAppointments();
  };

  const isVIP = (appointment) => {
    return VIPS.includes(appointment.vin)
  };

  return (
    <>
      <h1>Service Appointments</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer Name</th>
            <th>Appointment Date/Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Cancel</th>
            <th>Finish</th>

          </tr>
        </thead>
        <tbody>
          {appointments.map(appointment => {
            return (
              <tr key={appointment.id}>
                <td className="align-middle">{appointment.vin}</td>
                <td>{isVIP(appointment) ? 'Yes' : 'No'}</td>
                <td className="align-middle">{appointment.customer}</td>
                <td className="align-middle">{appointment.date_time}</td>
                <td className="align-middle">{appointment.technician.first_name}{appointment.technician.last_name}</td>
                <td className="align-middle">{appointment.reason}</td>
                <td><button className="align-middle btn btn-outline-danger" onClick={() => { clickCancel(appointment.id) }}>Cancel</button></td>
                <td><button className="align-middle btn btn-outline-success" onClick={() => { clickFinish(appointment.id) }}>Finish</button></td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </>
  );
}

export default AppointmentList;
