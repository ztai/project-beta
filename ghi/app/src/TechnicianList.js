import React, { useState, useEffect } from 'react';

const TechnicianList = () => {
  const [technicians, setTechnicians] = useState([]);

  const fetchTechnicians = async () => {
    const url = 'http://localhost:8080/api/technicians/';
    const response = await fetch(url);
    const data = await response.json();
    console.log(data)
    setTechnicians(data.technician);
  };

  useEffect(() => {
    fetchTechnicians();
  }, []);

  return (
    <div>
      <h1>Technicians</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Employee ID</th>
          </tr>
        </thead>
        <tbody>
          {technicians.map((technician) => (
            <tr key={technician.employee_id}>
              <td>{technician.first_name}</td>
              <td>{technician.last_name}</td>
              <td>{technician.employee_id}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default TechnicianList;
