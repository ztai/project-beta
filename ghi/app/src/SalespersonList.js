import React, { useEffect, useState } from 'react';

function SalespersonList() {
    const [salespeople, setSalespeople] = useState([])
    const fetchData = async () => {

        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople)
        }
    }

    useEffect(() => {
        fetchData()
    }, []);

    const clickDelete = async (id) => {
        const url = `http://localhost:8090/api/salespeople/${id}`;
        const fetchConfig = {
            method: "Delete",
        };
        const response = await fetch(url, fetchConfig);
        fetchData()
    }

    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Employee ID</th>
                        <th>Delete?</th>
                    </tr>
                </thead>
                <tbody>
                    {salespeople.map(salesperson => {
                        return (
                            <tr key={salesperson.employee_id}>
                                <td className="align-middle">{salesperson.first_name}</td>
                                <td className="align-middle">{salesperson.last_name}</td>
                                <td className="align-middle">{salesperson.employee_id}</td>
                                <td><button className="align-middle btn btn-outline-danger" onClick={() => { clickDelete(salesperson.id) }}>Delete</button></td>
                            </tr>
                        )
                    })
                    }
                </tbody>
            </table>
        </>
    );
}

export default SalespersonList;
