import React, { useEffect, useState } from 'react';

function SalesHistory() {
    const [sales, setSales] = useState([])
    const [salesDisplay,setSalesDisplay] = useState([])
    const [salespeople, setSalespeople] = useState([])

    const handleSalespersonChange = (event) => {
        setSalesDisplay(sales.filter(sale => sale.salesperson.employee_id==event.target.value));
    }

    const fetchData = async () => {

        const url = 'http://localhost:8090/api/sales/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales)
            setSalesDisplay(sales)
        }
        const salespersonUrl = 'http://localhost:8090/api/salespeople/';
        const salespersonResponse = await fetch(salespersonUrl);
        if (salespersonResponse.ok) {
            const data = await salespersonResponse.json();
            setSalespeople(data.salespeople)
        }
    }

    useEffect(() => {
        fetchData()
    }, []);


    return (
        <>
             <div className="mb-3 mt-3">
                                <select required onChange={handleSalespersonChange} name="salesperson" id="salesperson" className="form-select">
                                    <option value="">Choose a salesperson</option>
                                    {salespeople.map(salesperson => {
                                        return (
                                            <option key={salesperson.employee_id} value={salesperson.employee_id}>
                                                {salesperson.employee_id} - {salesperson.first_name} {salesperson.last_name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson Employee ID</th>
                        <th>Salesperson Name </th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {salesDisplay.map(sale => {
                        return (
                            <tr key={sale.automobile.vin}>
                                <td className="align-middle">{sale.salesperson.employee_id}</td>
                                <td className="align-middle">{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                                <td className="align-middle">{sale.customer.first_name} {sale.customer.last_name}</td>
                                <td className="align-middle">{sale.automobile.vin}</td>
                                <td className="align-middle">${sale.price}</td>
                            </tr>
                        )
                    })
                    }
                </tbody>
            </table>
        </>
    );
}

export default SalesHistory;
