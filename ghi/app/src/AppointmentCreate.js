import React, { useEffect, useState } from 'react';


function ServiceAppointmentCreate() {
  const [vin, setVin] = useState('');
  const [customer, setCustomer] = useState('');
  const [date_time, setdate_time] = useState('');
  // const [appointmentTime, setAppointmentTime] = useState('');
  const [technician, setTechnician] = useState('');
  const [reason, setReason] = useState('');
  const [technicians, setTechnicians] = useState([])


  const fetchData = async () => {
    const url = 'http://localhost:8080/api/technicians/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technician)
      console.log(data);
    }
  }

  useEffect(() => {
    fetchData();
  },[]);


  const handleSubmit = async (e) => {
    e.preventDefault();

    const data = {
      vin,
      customer,
      date_time,
      // appointmentTime,
      technician,
      reason
    };

    console.log(data)

    const url = 'http://localhost:8080/api/appointments/';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
     const response = await fetch(url, fetchConfig);
     console.log(response)
     if (response.ok) {
      setVin('');
      setCustomer('');
      setdate_time('');
      // setAppointmentTime('');
      setTechnician('');
      setReason('');
     }
  }

  const handleVinChange = (e) => {
    setVin(e.target.value);
  }

  const handleCustomerChange = (e) => {
    setCustomer(e.target.value);
  }

  const handledate_timeChange = (e) => {
    setdate_time(e.target.value);
  }

  // const handleAppointmentTimeChange = (e) => {
  //   setAppointmentTime(e.target.value);
  // }

  const handleTechnicianChange = (e) => {
    setTechnician(e.target.value);
  }

  const handleReasonChange = (e) => {
    setReason(e.target.value);
  }


  return (
    <div className='row'>
      <div className='offset-3 col-6'>
        <div className='shadow p-4 mt-4'>
          <h1>Create a Service Appointment</h1>
          <form onSubmit={handleSubmit}>
            <div className="form-group mb-3">
              <label htmlFor="vin">VIN</label>
              <input type="text" value={vin} onChange={handleVinChange} className="form-control" />
            </div>
            <div className="form-group mb-3">
              <label htmlFor="customer_name">Customer Name</label>
              <input type="text" value={customer} onChange={handleCustomerChange} className="form-control" />
            </div>
            <div className="form-group mb-3">
              <label htmlFor="appointment_date">Appointment Date/Time</label>
              <input type="datetime-local" value={date_time} onChange={handledate_timeChange} className="form-control" />
            </div>
            {/* <div className="form-group mb-3">
              <label htmlFor="appointment_time">Appointment Time</label>
              <input type="time" value={appointmentTime} onChange={handleAppointmentTimeChange} className="form-control" />
            </div> */}
            <div className="form-group mb-3">
              <label htmlFor="technician">Technician</label>
              <select value={technician} onChange={handleTechnicianChange} className="form-control">
                <option value="">Select a Technician</option>
                {technicians.map(technician => {
                    return (
                        <option key={technician.employee_id} value={technician.employee_id}>{technician.first_name}</option>
                    )
                })}
              </select>
            </div>
            <div className="form-group mb-3">
              <label htmlFor="reason">Reason</label>
              <input type="text" value={reason} onChange={handleReasonChange} className="form-control" />
            </div>
            <button type="submit" className="btn btn-primary">Create Appointment</button>
          </form>
        </div>
      </div>
    </div>
  );
}
export default ServiceAppointmentCreate;
