from django.db import models

# Create your models here.
class AutomobileVO(models.Model):
    vin=models.CharField(max_length=200,unique=True)
    sold=models.BooleanField(default=False)

class Salesperson(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=200,unique=True)

class Customer(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(unique=True, max_length=200)

class Sale(models.Model):
    price = models.FloatField()
    salesperson = models.ForeignKey(
        Salesperson,
        related_name="sale",
        on_delete=models.CASCADE
    )
    customer = models.ForeignKey(
        Customer,
        related_name="sale",
        on_delete=models.CASCADE
    )
    automobile = models.OneToOneField(
        AutomobileVO,
        related_name="sale",
        on_delete=models.CASCADE,
    )
