from django.shortcuts import render
from django.http import HttpResponseNotFound
# Create your views here.
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
import decimal

from common.json import ModelEncoder
from .models import AutomobileVO, Salesperson, Customer, Sale

class DecimalEncoder(json.JSONEncoder):
    def default(self,o):
        if isinstance(o, decimal.Decimal):
            return str(o)
        return super(DecimalEncoder, self).default(o)

class AutomobileVOEncoder(ModelEncoder):
    model=AutomobileVO
    properties = [
        "vin",
        "sold"
    ]

class SalespersonListEncoder(ModelEncoder):
    model=Salesperson
    properties =[
        "employee_id",
        "first_name",
        "last_name"
    ]

class CustomerListEncoder(ModelEncoder):
    model=Customer
    properties =[
        "address",
        "first_name",
        "last_name",
        "phone_number"
    ]

class SalesListEncoder(ModelEncoder):
    model=Sale
    properties =[
        "price",
        "customer",
        "salesperson",
        "automobile"
    ]
    encoders = {
        "customer": CustomerListEncoder(),
        "salesperson": SalespersonListEncoder(),
        "automobile": AutomobileVOEncoder(),
        "Decimal": DecimalEncoder()
    }

@require_http_methods(["GET","POST"])
def api_list_salespeople(request):
    if request.method == "GET":

        return JsonResponse(
            {"salespeople": Salesperson.objects.all()},
            encoder = SalespersonListEncoder,
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonListEncoder,
            safe=False
        )

@require_http_methods(["GET","PUT","DELETE"])
def api_show_salesperson(request, id):
    if request.method == "GET":
        salesperson = Salesperson.objects.get(id=id)
        return JsonResponse(
            salesperson,
            encoder=SalespersonListEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Salesperson.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count})
    else:
        content = json.loads(request.body)
        Salesperson.objects.filter(id=id).update(**content)
        salesperson = Salesperson.objects.get(id=id)
        return JsonResponse(
            salesperson,
            encoder = SalespersonListEncoder,
            safe=False
        )

@require_http_methods(["GET","POST","DELETE"])
def api_list_customers(request):
    if request.method == "GET":

        return JsonResponse(
            {"customers": Customer.objects.all()},
            encoder = CustomerListEncoder,
        )
    elif request.method == "DELETE":
        count, _ = Customer.objects.all().delete()
        if(count==0):
            return JsonResponse(
                {"message": "Could not find Customer of ID to delete.",
                 "status_code" : "404"}
            )
        return JsonResponse({"deleted": count})
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerListEncoder,
            safe=False
        )

@require_http_methods(["GET","PUT","DELETE"])
def api_show_customer(request, id):
    if request.method == "GET":
        customer = Customer.objects.get(employee_id=id)
        return JsonResponse(
            customer,
            encoder=CustomerListEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Customer.objects.filter(employee_id=id).delete()
        return JsonResponse({"deleted": count})
    else:
        content = json.loads(request.body)
        Customer.objects.filter(employee_id=id).update(**content)
        customer = Customer.objects.get(employee_id=id)
        return JsonResponse(
            customer,
            encoder = CustomerListEncoder,
            safe=False
        )

@require_http_methods(["GET","POST"])
def api_list_sales(request):
    if request.method == "GET":

        return JsonResponse(
            {"sales": Sale.objects.all()},
            encoder = SalesListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            content["customer"] = Customer.objects.get(phone_number=content["customer"])
            content["salesperson"] = Salesperson.objects.get(employee_id=content["salesperson"])
            content["automobile"] = AutomobileVO.objects.get(vin=content["automobile"])
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message":"Invalid Automobile"},
                status = 400
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message":"Invalid Customer"},
                status = 400
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message":"Invalid Salesperson"},
                status = 400
            )
        sales = Sale.objects.create(**content)
        print(sales)
        return JsonResponse(
            sales,
            encoder=SalesListEncoder,
            safe=False
        )

@require_http_methods(["GET","PUT","DELETE"])
def api_delete_sale(request, id):
    if request.method == "GET":
        sales = Sale.objects.get(id=id)
        return JsonResponse(
            sales,
            encoder=SalesListEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Sale.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count})
    else:
        content = json.loads(request.body)
        Sale.objects.filter(id=id).update(**content)
        sales = Sale.objects.get(id=id)
        return JsonResponse(
            sales,
            encoder = SalesListEncoder,
            safe=False
        )
