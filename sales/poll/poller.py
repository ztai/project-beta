import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

# Import models from sales_rest, here.
# from sales_rest.models import Something
from sales_rest.models import AutomobileVO


def poll(repeat=True):
    while True:
        print('Sales poller polling for data')
        try:
            # Write your polling logic, here
            # Do not copy entire file
            print("Attempting AutomobileVOs update.")
            pollUrl = "http://project-beta-inventory-api-1:8000/api/automobiles"
            response = requests.get(pollUrl)
            content = json.loads(response.content)
            vins = []
            for auto in content["autos"]:
                vins.append(auto["vin"])
                print(auto)
                if not (AutomobileVO.objects.filter(vin=auto["vin"])):
                    AutomobileVO.objects.create(**{"vin":auto["vin"],"sold": auto["sold"]})
                else:
                    AutomobileVO.objects.filter(vin=auto["vin"]).update(**{"sold":auto["sold"]})
            AutomobileVO.objects.exclude(vin__in=vins).delete()
            print("AutomobileVOs update attempted.")
            print(vins)
        except Exception as e:
            print(e, file=sys.stderr)

        if (not repeat):
            break

        time.sleep(60)


if __name__ == "__main__":
    poll()
